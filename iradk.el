;;; iradk.el --- Multi-radical lookup for Japanese kanji -*- lexical-binding: t -*-

;; Author: Walter Lewis
;; URL: https://codeberg.org/warp/iradk
;; Keywords: japanese

;; This file is part of iradk.
;;
;; iradk is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; iradk is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; iradk. If not, see <http://www.gnu.org/licenses/>.
;;; Code:

(defcustom iradk-file-r-to-k "/usr/share/edict/radkfile"
  "The radical->kanji dictionary file used by `iradk'."
  :type '(choice (const "/usr/share/edict/radkfile") string))

(defcustom iradk-file-k-to-r "/usr/share/edict/kradfile"
  "The kanji->radical dictionary file used by `iradk'."
  :type '(choice (const "/usr/share/edict/kradfile") string))

(defvar iradk-buffer "*iradk*")
(defvar iradk-results-buffer "*iradk results*")
(defvar iradk-mode-map (make-sparse-keymap))
(defvar iradk-rads nil)
(defvar iradk-opts nil)
(defvar iradk-kanji nil)
(defvar iradk-selected nil)

(defface iradk-face '((t :inherit shadow))
  "Face used for deselected radicals.")
(defface iradk-selected-face '((t :inherit highlight))
  "Face used for selected radicals.")
(defface iradk-opt-face `((t :inherit (shadow link)))
  "Face used for deselected radicals with an opt field.")
(defface iradk-selected-opt-face `((t :inherit (highlight link)))
  "Face used for selected radicals with an opt field.")

(define-minor-mode iradk-mode
  "Incremental multi-radical kanji lookup mode."
  :lighter " radk"
  :keymap iradk-mode-map)

(defun iradk-select (pos)
  (interactive "d")
  (let ((c (char-after pos))
        (inhibit-read-only t)
        (face (get-text-property pos 'face))
        (echo (get-text-property pos 'help-echo)))
    (when (memq c (string-to-list "0123456789 "))
      (error "Wrong character"))
    (if (memq c iradk-selected)
        (progn
          (setq iradk-selected (seq-remove (lambda (r) (equal r c)) iradk-selected))
          (set-text-properties
           pos (1+ pos)
           `(help-echo
             echo
             face
             ,(cond ((eq face 'iradk-selected-face) 'iradk-face)
                    ((eq face 'iradk-selected-opt-face) 'iradk-opt-face)
                    (face))))
          (message "deselected %s" (string c)))
      (setq iradk-selected (cons c iradk-selected))
      (set-text-properties
       pos (1+ pos)
       `(help-echo
         echo
         face
         ,(cond ((eq face 'iradk-face) 'iradk-selected-face)
                ((eq face 'iradk-opt-face) 'iradk-selected-opt-face)
                (face))))
      (message "selected %s" (string c)))
    (iradk-occur iradk-selected iradk-kanji)))

(defun iradk-occur (rads kanji)
  (interactive (list iradk-selected iradk-kanji))
  (if (null rads)
      (with-output-to-temp-buffer iradk-results-buffer
        (princ
         (string-join
          (mapcar
              (lambda (k/rs)
                (apply #'string (car k/rs) ?　 (cdr k/rs)))
            kanji)
          "\n")))
    (let ((rad (car rads)))
      (iradk-occur
       (cdr rads)
       (seq-filter (lambda (k/rs) (memq rad (cdr k/rs)))
         kanji)))))

(define-key iradk-mode-map (kbd "RET") 'iradk-select)
(define-key iradk-mode-map (kbd "<mouse-3>") 'iradk-select)

(defun iradk-load-rads ()
  (interactive)
  (setq iradk-rads nil iradk-opts nil)
  (setq rs nil strokes nil ks nil)
  (with-temp-buffer
    (insert-file-contents iradk-file-r-to-k)
    (decode-coding-region (point-min) (point-max) 'euc-jp)
    (while (re-search-forward "\\$ \\(.\\) \\([0-9]+\\)\\(?: \\(.*\\)\\)?\n\\([^$]+\\)" nil t)
      (let* ((rstr (match-string-no-properties 1))
             (r (when (= 1 (length rstr)) (string-to-char rstr)))
             (ss (string-to-number (match-string-no-properties 2)))
             (opt (match-string-no-properties 3)))
        (if (or (null strokes)
                (= ss strokes))
            (setq rs (cons r rs))
          (setq iradk-rads (cons (cons strokes rs) iradk-rads))
          (setq rs (list r)))
        (when (and opt (> (length opt) 0))
          (setq iradk-opts (cons (cons r opt) iradk-opts)))
        (setq strokes ss))))
  (setq iradk-rads (reverse (cons (cons strokes rs) iradk-rads)))
  (setq iradk-opts (reverse iradk-opts)))

(defun iradk-load-kanji ()
  (interactive)
  (with-temp-buffer
    (insert-file-contents iradk-file-k-to-r)
    (decode-coding-region (point-min) (point-max) 'euc-jp)
    (re-search-forward "^[^#]")
    (backward-char)
    (setq
     iradk-kanji
     (mapcar
         (lambda (str)
           (cons (string-to-char str)
                 (seq-remove (lambda (c) (eq ?  c))
                             (string-to-list (substring str 4)))))
       (split-string
        (substring-no-properties
         (buffer-substring (point) (point-max)))
        "\n" t)))))

(defun iradk ()
  (interactive)
  (when (buffer-live-p (get-buffer iradk-buffer))
    (kill-buffer iradk-buffer))
  (setq iradk-selected nil)
  (iradk-load-rads)
  (iradk-load-kanji)
  (pop-to-buffer iradk-buffer)
  (insert
   (string-join
    (mapcar #'iradk-format-rads iradk-rads)))
  (goto-char (+ 3 (point-min)))
  (read-only-mode 1)
  (iradk-mode 1))

(defun iradk-format-rads (ss/rs)
  (format "%2d %s\n"
    (car ss/rs)
    (mapconcat
        (lambda (r)
          (apply #'propertize
            (string r)
            (let ((opt (assq r iradk-opts)))
              (if (null opt)
                  '(face iradk-face)
                `(face iradk-opt-face help-echo ,(cdr opt))))))
      (cdr ss/rs)
      "")))

;;; iradk.el ends here
